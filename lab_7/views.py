from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from django.forms.models import model_to_dict
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Friend, Mahasiswa
from api_csui_helper.csui_helper import CSUIhelper
import os
import json

response = {}
csui_helper = CSUIhelper()


def index(request):
    # Page halaman menampilkan list mahasiswa yang ada
    # TODO berikan akses token dari backend dengan menggunakaan helper yang ada

    mahasiswa_list = csui_helper.instance.get_mahasiswa_list()
    for data in mahasiswa_list:
        nama = data['nama']
        npm = data['npm']
        Mahasiswa.objects.get_or_create(nama_mahasiswa=nama, npm=npm)
    
    mahasiswa_list = Mahasiswa.objects.all()
    paginator = Paginator(mahasiswa_list, 25)
    page = request.GET.get('page')
    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)

    friend_list = Friend.objects.all()
    response = {"mahasiswa_list": data, "friend_list": friend_list}
    html = 'lab_7/lab_7.html'
    response["author"] = "Claudio Yosafat"
    return render(request, html, response)


def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    response["author"] = "Claudio Yosafat"
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']
        print(name)
        print(npm)
        checked= isChecked(npm)
        data = {"isChecked" : checked}

        if(data["isChecked"]):
            friend = Friend(friend_name=name, npm=npm)
            friend.save()
            data['friend'] = model_to_dic(friend)

        return JsonResponse(data)

def my_friend_list(request):
    friends = Friend.objects.all()
    lst = []

    for friend in friends:
        lst.append(model_to_dic(friend))

    listTeman = {'list' : lst}
    return JsonResponse(listTeman)

@csrf_exempt
def delete_friend(request):
    if request.method == "POST":
        friend_id = request.POST['id']
        Friend.objects.filter(id=friend_id).delete()
        data = {'id' : friend_id}
        return JsonResponse(data)

def isChecked(npm):
    friends = Friend.objects.all()

    for friend in friends:
        if friend.npm == npm:
            return False

    return True

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    data = {
        'is_taken': not isChecked(npm) #lakukan pengecekan apakah Friend dgn npm tsb sudah ada
    }
    return JsonResponse(data)

def model_to_dic(obj):
    return model_to_dict(obj)


