from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from .models import Friend, Mahasiswa


# Create your tests here.

class Lab7UnitTest(TestCase):
    def test_lab_7_url_is_exist(self):
        response = Client().get('/lab-7/')
        self.assertEqual(response.status_code, 200)

    def test_lab_7_using_index_func(self):
        found = resolve('/lab-7/')
        self.assertEqual(found.func, index)

    # def test_root_url_now_is_using_index_page_from_lab_7(self):
    #     response = Client().get('/')
    #     self.assertEqual(response.status_code, 301)
    #     self.assertRedirects(response,'/lab-7/',301,200)
        

    def test_daftar_teman_url_is_exist(self):
        response = Client().get('/lab-7/get-friend-list/')
        self.assertEqual(response.status_code, 200)

    def test_lab7_add_friend_success(self):
        nama = 'Claudio Yosafat'
        npm = '1606917664'
        data = {'name': nama, 'npm': npm}
        response_post = Client().post('/lab-7/add-friend/', data)
        self.assertEqual(response_post.status_code, 200)

    def test_delete_friend_success(self):
        new_friend = Friend.objects.create(friend_name='Claudio Yosafat', npm='1606917664')
        response = Client().post('/lab-7/delete-friend/', {'id':new_friend.id})
        self.assertEqual(response.status_code, 200)

    def test_npm_is_checked(self):
        new_friend = Friend.objects.create(friend_name='Claudio Yosafat', npm='1606917664')
        self.assertFalse(isChecked('1606917664'))
        self.assertTrue(isChecked('0000000'))

    def test_validate_npm(self):
        new_friend = Friend.objects.create(friend_name='Claudio Yosafat', npm='1606917664')
        response = Client().post('/lab-7/validate-npm/', {'npm': new_friend.npm})
        self.assertEqual(dict, type(response.json()))

    def test_get_my_friend_list(self):
        new_friend = Friend.objects.create(friend_name='Claudio Yosafat', npm='160617664')
        response = Client().post('/lab-7/my_friend_list/')
        self.assertEqual(dict, type(response.json()))

    def test_invalid_page(self):
        response = Client().get('/lab-7/?page=*')
        self.assertRaises(PageNotAnInteger)

    def test_empty_page(self):
        response = Client().get('/lab-7/?page=-9')
        self.assertRaises(EmptyPage)
