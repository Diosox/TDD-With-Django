from django.shortcuts import render

response = {}
def index(request):    
    response['author'] = "Claudio Yosafat"
    html = 'lab_8/lab_8.html'
    return render(request, html, response)

