// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    print.value = "";
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END

//Storage Theme
var themes = [
  {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
  {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
  {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
  {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
  {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
  {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
  {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
  {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
  {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
  {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
  {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
];

var selectedTheme = {"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}};

//LocalStorage
if(localStorage.getItem("themes") === null)
{
  localStorage.setItem("themes", JSON.stringify(themes));
}
if(localStorage.getItem("selectedTheme") === null)
{
  localStorage.setItem("selectedTheme", JSON.stringify(selectedTheme["Indigo"]));
}

function changeTheme(theme){
  $("body").css("background-color", theme.bcgColor);
  // $("body").css("color",theme.fontColor);
}

//Default Theme
changeTheme(JSON.parse(localStorage.getItem("selectedTheme")));

$(document).ready(function() {
    $('.my-select').select2({
    'data': JSON.parse(localStorage.getItem("themes"))
    });

    var themes = JSON.parse(localStorage.getItem("themes"));
    $('.apply-button').on('click', function(){ 
    // [TODO] ambil value dari elemen select .my-select
    var value = $(".my-select").val();

    // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada
    for(i = 0; i < themes.length; i++)
    {
      if(themes[i].id == value)
      {
          // [TODO] ambil object theme yang dipilih
          var change = themes[i];
      }
    }

    // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
    changeTheme(change);

    // [TODO] simpan object theme tadi ke local storage selectedTheme
    localStorage.setItem("selectedTheme", JSON.stringify(change));
    })

});

//Chat Function
var counter = 0;
$(document).ready(function() {
  $("#chat").keypress(function(keyboards) {
    if(keyboards.which == 13) //13 itu 'enter'
    {
      if(counter % 2 == 0)
      {
        $('.msg-insert').append("<div class= \"msg-send\">" + $('#chat').val() + '</div>');
      }
      else
      {
        $('.msg-insert').append("<div class= \"msg-receive\">" + $('#chat').val() + '</div>');
      }

      counter++;
      $('#chat').val('');
      event.preventDefault(); //mengembalikan textare menjadi normal
    }

  });
});

