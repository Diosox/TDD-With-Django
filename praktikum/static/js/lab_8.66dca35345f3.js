// FB initiation function
window.fbAsyncInit = () => {
  FB.init({
    appId: '1950139291976279',
    cookie: true,
    xfbml: true,
    version: 'v2.11'
  });

  FB.getLoginStatus((response) => {
    // console.log(response.status, 'UHUY');
    render(response.status);
  });
};


// Call init facebook. default dari facebook
(function (d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) { return; }
  js = d.createElement(s); js.id = id;
  js.src = "https://connect.facebook.net/en_US/sdk.js";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

const render = loginFlag => {
  if (loginFlag === 'connected') {
    console.log("masuk TRUEnya render");
    getUserData(user => {
      console.log('masuk getUserData render');
      console.log(user);
      // Render tampilan profil, form input post, tombol post status, dan tombol logout
      $('#lab8').html(
        '<div class="profile">' +
        '<img class="cover" src="' + user.cover.source + '" alt="cover" />' +
        '<div class="data">' +
        '<h1>' + user.name + '</h1>' +
        '<h2>' + user.about + '</h2>' +
        '<h3>' + user.email + ' - ' + user.gender + '</h3>' +
        '</div>' +
        '</div>' +
        '<input id="postInput" type="text" class="post" placeholder="Ketik Status Anda" />' +
        '<button class="postStatus" onclick="postStatus()">Post ke Facebook</button>' +
        '<button class="logout" onclick="facebookLogout()">Logout</button>'
      );

      getUserFeed(feed => {
        console.log('punya getuserfeed render');
        console.log(feed);
        feed.data.map(value => {
          // Render feed, kustomisasi sesuai kebutuhan.
          if (value.message && value.story) {
            $('#status').append(
              '<div class="feed">' +
              '<div class="smallPic"> <img src="' + user.picture.data.url + '" alt="smallpic"/>' + user.name + '</div>' +
              '<input type="button" class="deleteBtn" onclick="deleteFeed(event)" id="' + value.id + '" value="Delete this post" ></input>' +
              '<div>' + value.message + '</div>' +
              '</div>'

            );
          } else if (value.message) {
            $('#status').append(
              '<div class="feed">' +
              '<div class="smallPic"> <img src="' + user.picture.data.url + '" alt="smallpic"/>' + user.name + '</div>' +
              '<input type="button" class="deleteBtn" onclick="deleteFeed(event)" id="' + value.id + '" value="Delete this post" ></input>' +
              '<div>' + value.message + '</div>' +
              '</div>'

            );
          } else if (value.story) {
            $('#status').append(
              '<div class="feed">' +
              '<div class="smallPic"> <img src="' + user.picture.data.url + '" alt="smallpic"/>' + user.name + '</div>' +
              '<input type="button" class="deleteBtn" onclick="deleteFeed(event)" id="' + value.id + '" value="Delete this post"></input>' +
              '<div>' + value.story + '</div>' +
              '</div>'

            );
          }
        });
      });
    });
  } else {
    console.log("masuk ELSEnya render");
    // Tampilan ketika belum login
    $("#lab8").html(
      '<button class="login" onclick="facebookLogin()">Login</button>'
    );
    $("#status").empty();
  }
};

const facebookLogin = () => {
  console.log('-------');
  console.log("masuk facebookLogin");
  FB.login(function (response) {
    console.log(response.status, 'ini response.statusnya facebookLogin');
    render(response.status);
  }, { scope: 'public_profile,user_posts,publish_actions,user_about_me,user_birthday,email,user_friends' })

};

const facebookLogout = () => {
  console.log('MASUK GAK');
  FB.getLoginStatus(function (response) {
    console.log(response.status === 'connected');
    if (response.status === 'connected') {
      FB.logout();
      console.log(response.status);
      render(false);
    }
  });
};

const getUserData = (fun) => {
  FB.getLoginStatus(function (response) {
    if (response.status === 'connected') {
      FB.api('/me?fields=id,name,cover,picture,email,gender,about,address', 'GET', function (response) {
        console.log('data-data');
        console.log(response);
        fun(response);
      });
    }
  });
};

const getUserFeed = (fun) => {
  console.log('masuk getUserFeed');
  FB.api(
    '/me/feed',
    function (response) {
      if (response && !response.error) {
        console.log(response);
        console.log("==============");
        fun(response);
      }
    }
  )
};

const postFeed = (message) => {
  console.log('masuk postFeed');
  FB.api('/me/feed',
    'POST',
    {
      message: message
    },
    function (response) {
      if (response && !response.error) {
        getUserData( (user) =>{
          console.log(user);
          $('#status').prepend(
            '<div class="feed">' +
            '<div class="smallPic"> <img src="'+user.picture.data.url+'" alt="smallpic"/>'+user.name+'</div>'+
            '<input type="button" class="deleteBtn" onclick="deleteFeed(event)" id="'+response.id+'" value="Delete this post" ></input>'+
            '<div>' + message + '</div>' +
            '</div>'
            );
        });
        console.log('success post');
      }
    });
};

const postStatus = () => {
  const message = $('#postInput').val();
  $('#postInput').val('');
  event.preventDefault();
  postFeed(message);
};

const removePost = (status) => {
  FB.api(
    '/me/feed',
    "DELETE",
    function (response) {
      if (response && !response.error) {
        console.log('success delete');
      }
    }
  );
};


