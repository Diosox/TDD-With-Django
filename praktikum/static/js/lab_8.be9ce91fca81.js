// FB initiation function
window.fbAsyncInit = () => {
  FB.init({
    appId: '1950139291976279',
    cookie: true,
    xfbml: true,
    version: 'v2.11'
  });

  FB.getLoginStatus((response) => {
    // console.log(response.status, 'UHUY');
    render(response.status);
  });
};


// Call init facebook. default dari facebook
(function (d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) { return; }
  js = d.createElement(s); js.id = id;
  js.src = "https://connect.facebook.net/en_US/sdk.js";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

const render = loginFlag => {
  if (loginFlag === 'connected') {
    console.log("masuk TRUEnya render");
    getUserData(user => {
      console.log('masuk getUserData render');
      console.log(user);
      // Render tampilan profil, form input post, tombol post status, dan tombol logout
      $('#lab8').html(
        '<div class="photoContainer" style="background-image: url(' + user.cover.source + ')">' +
        '<div class="item1"> ' +
        '<img class="picture" src="https://scontent.xx.fbcdn.net/v/t1.0-1/p200x200/14938250_1536952609665263_8331465695488377642_n.jpg?oh=c5fe8fb987ad200948812d950d71ccbd&oe=5A8FA05E" alt="profpic" />' +
        '<span class="userName">' + user.name + '</span>' +
        '</div>' +
        '<div class="item2">' +
        '<b>About me</b>' +
        '<div>' + user.about + '</div>' +
        '<b>E-mail</b>' +
        '<div>' + user.email + '</div>' +
        '<b>Gender</b>' +
        '<div>' + user.gender + '</div>' +
        '</div>' +
        '</div>' +
        '<div class="postFeed">' +
        '<textarea id="postInput" class="post" placeholder="Ketik Status Anda" />' +
        '<button class="postStatus" onclick="postStatus()">Post Status</button>' +
        '<button class="logout" onclick="facebookLogout()">Logout</button>' +
        '</div>'

      );

      getUserFeed(feed => {
        console.log('punya getuserfeed render');
        console.log(feed);
        feed.data.map(value => {
          // Render feed, kustomisasi sesuai kebutuhan.
          if (value.message && value.story) {
            $('#status').append(
              '<div class="feed">' +
              '<div class="smallPic"> <img src="' + user.picture.data.url + '" alt="smallpic"/>' + user.name + '</div>' +
              '<input type="button" class="deleteBtn" onclick="deleteFeed(event)" id="' + value.id + '" value="Delete this post" ></input>' +
              '<div>' + value.message + '</div>' +
              '</div>'
            );
          } else if (value.message) {
            $('#status').append(
              '<div class="feed">' +
              '<div class="smallPic"> <img src="' + user.picture.data.url + '" alt="smallpic"/>' + user.name + '</div>' +
              '<input type="button" class="deleteBtn" onclick="deleteFeed(event)" id="' + value.id + '" value="Delete this post" ></input>' +
              '<div>' + value.message + '</div>' +
              '</div>'
            );
          } else if (value.story) {
            $('#status').append(
              '<div class="feed">' +
              '<div class="smallPic"> <img src="' + user.picture.data.url + '" alt="smallpic"/>' + user.name + '</div>' +
              '<input type="button" class="deleteBtn" onclick="deleteFeed(event)" id="' + value.id + '" value="Delete this post"></input>' +
              '<div>' + value.story + '</div>' +
              '</div>'

            );
          }
        });
      });
    });
  } else {
    console.log("masuk ELSEnya render");
    // Tampilan ketika belum login
    $('#lab8').html(
      '<div class="welcomeText"><span id="noEnter1">You haven\'t signed in yet?</span><br><span id="noEnter"> Do it then.</span></div>'+'<button href="#" onclick="facebookLogin()" class="login">Login</button>' 
      );
    $('#status').empty();
  }
};

const facebookLogin = () => {
  console.log('-------');
  console.log("masuk facebookLogin");
  FB.login(function (response) {
    console.log(response.status, 'ini response.statusnya facebookLogin');
    render(response.status);
  }, { scope: 'public_profile,user_posts,publish_actions,user_about_me,user_birthday,email,user_friends' })

};

const facebookLogout = () => {
  console.log('MASUK GAK');
  FB.getLoginStatus(function (response) {
    console.log(response.status === 'connected');
    if (response.status === 'connected') {
      FB.logout();
      console.log(response.status);
      render(false);
    }
  });
};

const getUserData = (fun) => {
  FB.getLoginStatus(function (response) {
    if (response.status === 'connected') {
      FB.api('/me?fields=id,name,cover,picture,email,gender,about,address', 'GET', function (response) {
        console.log('data-data');
        console.log(response);
        fun(response);
      });
    }
  });
};

const getUserFeed = (fun) => {
  console.log('masuk getUserFeed');
  FB.api(
    '/me/feed',
    function (response) {
      if (response && !response.error) {
        console.log(response);
        console.log("==============");
        fun(response);
      }
    }
  )
};

const postFeed = (message) => {
  console.log('masuk postFeed');
  FB.api('/me/feed',
    'POST',
    {
      message: message
    },
    function (response) {
      if (response && !response.error) {

        console.log('success post');
      }
    });
};

const postStatus = () => {
  const message = $('#postInput').val();
  $('#postInput').val('');
  event.preventDefault();
  postFeed(message);
};

const removePost = (status) => {
  FB.api(
    '/me/feed',
    "DELETE",
    function (response) {
      if (response && !response.error) {
        console.log('success delete');
      }
    }
  );
};


